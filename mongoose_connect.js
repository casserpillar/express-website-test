var mongoose = require('mongoose');

var user = process.env.MONGOOSE_USER
var pass = process.env.MONGOOSE_PASS
var db = mongoose.connect('mongodb://' + user + ':' + pass + '@ds139278.mlab.com:39278/cassandrafyi-recipes');
var Schema = mongoose.Schema;
var recipeSchema = new Schema({
    name: String,
    serves: Number,
    ingredients: [{name: String, contents: [String]}],
    tools: [String],
    recipemethod: [{name: String, contents: [String]}]
});

module.exports.recipeModel = mongoose.model('cassandrafyi-recipe', recipeSchema);
