var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var Schema = mongoose.Schema;
var mongooseConfig = require("../mongoose_connect.js")
var Recipe = mongooseConfig.recipeModel;

/* GET lasagna. */
router.get('/', function(req, res, next) {
	Recipe.find(function (err, recipeList) {
		if (err) {
			res.render(err);
		}
		else {
			res.render('recipeList', {recipeList: recipeList});
		}
	});
});

router.get('/:recipeName', function(req, res, next) {
	Recipe.findOne({"name": req.params.recipeName}, function(err, recipe) {
		if (err) {
			res.render(err)
		} 
		else {
			res.render('recipe', {recipe: recipe});
		}
	})
})

module.exports = router;
