var express = require('express');
var router = express.Router();
app.use(express.static('data'))

/* GET pdf file. */
router.get('/resume', function(req, res) {
  res.sendFile(__dirname + 'data/resume.pdf', 'resume.pdf');
});

module.exports = router;
